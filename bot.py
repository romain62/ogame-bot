# main py file
import random
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import logging
import os
from logging.handlers import RotatingFileHandler
from config import options
from custom_sleep import custom_sleep
import time
from worker import Worker

class Bot(object):

    LOGIN_URL = 'https://'+options['credentials']['server'] + '.ogame.gameforge.com/'
    LOGGING_LEVEL = logging.INFO
    BINARY = FirefoxBinary(options['general']['browser_path'])
    DRIVERPATH = options['general']['driver_path']
    VERSION= "1.1"

    def __init__(self):
        self.logged_in = False
        self._prepare_logger()
        self.driver = webdriver.Firefox(executable_path=self.DRIVERPATH,firefox_binary=self.BINARY)
        self.driver.implicitly_wait(options['general']['timeout'])
        self.lobby_tab = ''
        self.worker_list = []

    def _prepare_logger(self):
        self.logger = logging.getLogger("selenium")
        fh = RotatingFileHandler('bot.log', maxBytes=1000000, backupCount=5)
        sh = logging.StreamHandler()
        fmt = logging.Formatter(fmt='%(asctime)s %(levelname)s %(message)s',
                                datefmt='%d-%m, %H:%M:%S')
        fh.setFormatter(fmt)
        sh.setFormatter(fmt)
        self.logger.setLevel(self.LOGGING_LEVEL)
        self.logger.addHandler(fh)
        self.logger.addHandler(sh)

    def login(self):
        self.driver.refresh()
        if "index.php" in self.driver.current_url:
            self.logged_in = True
            self.logger.info('Already logged')
            return True
        else:
            if len(self.driver.window_handles) > 1:
                for tab in self.driver.window_handles:
                    if len(self.driver.window_hanldes > 1): #check if its the last tab
                        self.driver.switch_to_window(tab)
                        self.driver.close()

            self.logger.info('Loggin')
            user = options['credentials']['user']
            passw = options['credentials']['pass']
            #load login url
            self.driver.get(self.LOGIN_URL)
            #click in login tab
            self.driver.find_element_by_id('ui-id-1').click()
            #fill the login form and submit
            self.driver.find_element_by_id('usernameLogin').send_keys(user)
            self.driver.find_element_by_id('passwordLogin').send_keys(passw)
            self.driver.find_element_by_id('loginSubmit').click()

            #open all universes
            self.logger.info("Logged")
            #store the current tab
            for handle in self.driver.window_handles:
                self.lobby_tab = handle
            #this is very ugly and maybe could be improved 
            accounts = self.driver.find_element_by_id('accountlist')
            for i, item in enumerate(accounts.find_elements_by_class_name('btn-primary'), start=0):
                for j, button in enumerate(accounts.find_elements_by_class_name('btn-primary'), start=0):
                    if i == j:
                        button.click()
                        self.logger.info("Opening account")
                        self.driver.switch_to_window(self.lobby_tab)
                        self.worker_list.append(Worker(self.driver,self.driver.window_handles[1],self.logger,options))#last opened tab(between the lobby and the other tabs, this could be improved
                        time.sleep(4) #this could be improved
                        accounts = self.driver.find_element_by_id('accountlist')
                        break

            self.logged_in = True
            return True

    def handle_accounts(self):
        #make bot things
        for tabworker in self.worker_list:
            tabworker.work()
            self.logger.info('Executing worker')

    def sleep(self):
        sleep_options = options['general']
        sleep_time = random.randint(0, int(sleep_options['seed']))+int(sleep_options['check_interval'])
        self.logger.info('Sleeping for %s seconds' % sleep_time)
        custom_sleep(sleep_time,self.driver)

    def stop(self):
        self.logger.info('Stopping bot')
        os.unlink(self.pidfile)
        self.driver.quit()

    def start(self):
        self.logger.info('Starting bot version %s' % self.VERSION)
        self.pid = str(os.getpid())
        self.pidfile = 'bot.pid'
        f = open(self.pidfile, 'w')
        f.write(self.pid)

        #main loop
        while True:
            if self.login():
                self.handle_accounts()
            else:
                self.logger.error('Login failed!')
                self.stop()
                #return
            self.sleep()

if __name__ == "__main__":
    bot = Bot()
    bot.start()
