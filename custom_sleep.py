import time

def custom_sleep(sleep_time, driver):
    if(sleep_time < 3):
        sleep(sleep_time)
    else:
        driver.switch_to.window(driver.window_handles[0])
        driver.execute_script("window.open('');")
        driver.switch_to.window(driver.window_handles[1])
        driver.get("https://gnu.org")
        iterations = sleep_time/3
        while (iterations > 1):
            driver.switch_to.window(driver.window_handles[0])
            driver.switch_to.window(driver.window_handles[1])
            time.sleep(3)
            iterations = iterations - 1
        driver.close()
        driver.switch_to_window(driver.window_handles[-1])
