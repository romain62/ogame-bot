import time
from sim import Sim

class Worker():
    
    def __init__(self, driver,workertab,logger,options):
        self.sim = Sim()
        self.driver = driver
        self.worker_tab = workertab
        self.logger = logger
        self.options = options
        self.planet_id = []
        self.resources = {}
        self.structures = {}
        self.station_structures = {}

    def fetch_planets(self):
        planet_list = self.driver.find_elements_by_xpath("//div[contains(@id, 'planet-')]")
        for planet in planet_list:
            self.planet_id.append(planet.get_attribute("id"))

    def navigate_to_planet(self, planetid):
        self.driver.find_element_by_id(planetid).click()
        time.sleep(4)#this should be improved

    def update_planet_info(self, planet):
        self.logger.info("UPDATING PLANET INFO")
        #first update the available resources
        self.resources["metal"] = int(self.driver.find_element_by_id("resources_metal").text.replace(".",""))
        self.resources["crystal"] = int(self.driver.find_element_by_id("resources_crystal").text.replace(".",""))
        self.resources["deuterium"] = int(self.driver.find_element_by_id("resources_deuterium").text.replace(".",""))
        self.resources["energy"] = int(self.driver.find_element_by_id("resources_energy").text.replace(".",""))
        self.logger.info(self.resources)

    def get_mines_status(self, planet):
        structure1 = {'name': 'metalMine', 'buttonid':'button1'}
        mine_element = self.driver.find_element_by_id(structure1['buttonid'])
        structure1['level'] = int(mine_element.find_element_by_class_name("level").text)
        structure1['energyneeded'] = self.sim.upgrade_energy_cost(structure1['name'],structure1['level']+1)
        if mine_element.get_attribute("class") == "on":
            structure1['upgradeable'] = 1
        else:
            structure1['upgradeable'] = 0
        self.structures[structure1['name']] = structure1

        structure2 = {'name':'crystalMine', 'buttonid':'button2'}
        mine_element = self.driver.find_element_by_id(structure2['buttonid'])
        structure2['level'] = int(mine_element.find_element_by_class_name("level").text)
        structure2['energyneeded'] = self.sim.upgrade_energy_cost(structure2['name'],structure2['level']+1)
        if mine_element.get_attribute("class") == "on":
            structure2['upgradeable'] = 1
        else:
            structure2['upgradeable'] = 0
        self.structures[structure2['name']] = structure2

        structure3 = {'name':'deuteriumMine', 'buttonid':'button3'}
        mine_element = self.driver.find_element_by_id(structure3['buttonid'])
        structure3['level'] = int(mine_element.find_element_by_class_name("level").text)
        structure3['energyneeded'] = self.sim.upgrade_energy_cost(structure3['name'],structure3['level']+1)
        if mine_element.get_attribute("class") == "on":
            structure3['upgradeable'] = 1
        else:
            structure3['upgradeable'] = 0
        self.structures[structure3['name']] = structure3

        structure4 = {'name':'solarPlant', 'buttonid':'button4'}
        mine_element = self.driver.find_element_by_id(structure4['buttonid'])
        structure4['level'] = int(mine_element.find_element_by_class_name("level").text)
        structure4['energyneeded'] = 0 
        if mine_element.get_attribute("class") == "on":
            structure4['upgradeable'] = 1
        else:
            structure4['upgradeable'] = 0
        self.structures[structure4['name']] = structure4

        structure5 = {'name':'fusionPlant', 'buttonid':'button5'}
        mine_element = self.driver.find_element_by_id(structure5['buttonid'])
        structure5['level'] = int(mine_element.find_element_by_class_name("level").text)
        structure5['energyneeded'] = 0
        if mine_element.get_attribute("class") == "on":
            structure5['upgradeable'] = 1
        else:
            structure5['upgradeable'] = 0
        self.structures[structure5['name']] = structure5

    def upgrade(self, structure):
        if self.is_building_queue_empty():
            button_element = self.driver.find_element_by_id(structure['buttonid'])
            button_element.find_element_by_class_name("fastBuild.tooltip.js_hideTipOnMobile").click()

            self.logger.info('Upgrading %s' % structure['name'])

    def upgrade_energy(self):
        if self.structures['solarPlant']['upgradeable'] == 1:
            self.upgrade(self.structures['solarPlant'])
        elif self.structures['fusionPlant']['upgradeable'] == 1:
            if int(self.options['building']['max_fusion_plant_level']) > self.structures['fusionPlant']['level']:
                self.upgrade(self.structures['fusionPlant'])

    def choose_mine_upgrade(self):
        if int(self.options['building']['min_energy_level']) > self.resources['energy']:
            self.upgrade_energy()

        else:
            if int(self.options['building']['levels_diff'][2]) < (self.structures['metalMine']['level'] - self.structures['deuteriumMine']['level']):
                if self.structures['deuteriumMine']['upgradeable'] == 1:
                    if self.structures['deuteriumMine']['energyneeded'] < (self.resources['energy'] - int(self.options['building']['min_energy_level'])):
                        self.upgrade(self.structures['deuteriumMine'])
                    else:
                        self.upgrade_energy()

            if int(self.options['building']['levels_diff'][0]) < (self.structures['metalMine']['level'] - self.structures['crystalMine']['level']):
                if self.structures['crystalMine']['upgradeable'] == 1:
                    if self.structures['crystalMine']['energyneeded'] < (self.resources['energy'] - int(self.options['building']['min_energy_level'])):
                        self.upgrade(self.structures['crystalMine'])
                    else:
                        self.upgrade_energy()

            if self.structures['metalMine']['upgradeable'] == 1:
                if self.structures['metalMine']['energyneeded'] < (self.resources['energy'] - int(self.options['building']['min_energy_level'])):
                    self.upgrade(self.structures['metalMine'])
                else:
                    self.upgrade_energy()

    def choose_station_upgrade(self):
        self.get_stations_status()
        if self.station_structures['roboticsFactory']['upgradeable'] == 1:
            self.upgrade(self.station_structures['roboticsFactory'])
        elif self.station_structures['shipyard']['upgradeable'] == 1:
            self.upgrade(self.station_structures['shipyard'])
        elif self.station_structures['researchLab']['upgradeable'] == 1:
            self.upgrade(self.station_structures['researchLab'])
        elif self.station_structures['naniteFactory']['upgradeable'] == 1:
            self.upgrade(self.station_structures['naniteFactory'])
        else:
            self.logger.info('Not enough resources to upgrade') 

    def get_stations_status(self):
        structure = {'name':'roboticsFactory', 'buttonid': 'button0'}
        structure_element = self.driver.find_element_by_id(structure['buttonid'])
        if structure_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0 

        self.station_structures[structure['name']] = structure

        structure ={'name':'shipyard', 'buttonid': 'button1'}
        structure_element = self.driver.find_element_by_id(structure['buttonid'])
        if structure_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0

        self.station_structures[structure['name']] = structure

        structure ={'name':'researchLab', 'buttonid': 'button2'}
        structure_element = self.driver.find_element_by_id(structure['buttonid'])
        if structure_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0

        self.station_structures[structure['name']] = structure

        structure ={'name':'naniteFactory', 'buttonid': 'button5'}
        structure_element = self.driver.find_element_by_id(structure['buttonid'])
        if structure_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0

        self.station_structures[structure['name']] = structure

    def is_building_queue_empty(self):
        building_queue_element = self.driver.find_element_by_class_name('content-box-s')
        self.driver.switch_to.default_content()
        building_element = building_queue_element.find_elements_by_class_name('data')
        if len(building_element) > 0: #if the data element appears the queue is not empty
            return False
        else:
            return True

    def get_storage_status(self):
        structure = {'name':'metalStorage', 'buttonid': 'button7'}
        storage_element = self.driver.find_element_by_id(structure['buttonid'])
        structure['level'] = int(storage_element.find_element_by_class_name("level").text)
        structure['capacity'] = int(self.sim.get_capacity(structure['level']))
        structure['free'] = max(int(structure['capacity']) - int(self.resources['metal']),0)
        if self.resources['metal'] > structure['capacity']:
            structure['percent_free'] = 0
        else:
            structure['percent_free'] = 100-(self.resources['metal']/structure['capacity']*100)

        if storage_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0

        self.structures[structure['name']] = structure

        structure = {'name':'crystalStorage', 'buttonid': 'button8'}
        storage_element = self.driver.find_element_by_id(structure['buttonid'])
        structure['level'] = int(storage_element.find_element_by_class_name("level").text)
        structure['capacity'] = int(self.sim.get_capacity(structure['level']))
        structure['free'] = max(int(structure['capacity']) - int(self.resources['crystal']),0)
        if self.resources['crystal'] > structure['capacity']:
            structure['percent_free'] = 0
        else:
            structure['percent_free'] = 100-(self.resources['crystal']/structure['capacity']*100)
        if storage_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0

        self.structures[structure['name']] = structure

        structure = {'name':'deuteriumStorage', 'buttonid': 'button9'}
        storage_element = self.driver.find_element_by_id(structure['buttonid'])
        structure['level'] = int(storage_element.find_element_by_class_name("level").text)
        structure['capacity'] = int(self.sim.get_capacity(structure['level']))
        structure['free'] = max(int(structure['capacity']) - int(self.resources['deuterium']),0)
        if self.resources['deuterium'] > structure['capacity']:
            structure['percent_free'] = 0
        else:
            structure['percent_free'] = 100-(self.resources['deuterium']/structure['capacity']*100)

        if storage_element.get_attribute("class") == "on":
            structure['upgradeable'] = 1
        else:
            structure['upgradeable'] = 0
        self.structures[structure['name']] = structure

    def choose_storage_upgrade(self):
       if self.structures['metalStorage']['percent_free'] < int(self.options['building']['min_storage_free'].split(',')[0]):
            self.upgrade(self.structures['metalStorage'])
            return True
       elif self.structures['crystalStorage']['percent_free'] < int(self.options['building']['min_storage_free'].split(',')[1]):
            self.upgrade(self.structures['crystalStorage'])
            return True
       elif self.structures['deuteriumStorage']['percent_free'] < int(self.options['building']['min_storage_free'].split(',')[2]):
            self.upgrade(self.structures['deuteriumStorage'])
            return True
       else:
            return False

    def should_upgrade_storage(self):
        self.get_storage_status()
        return self.choose_storage_upgrade()

    def update_planet_resources(self, planet):
        if self.options['building']['enabled'] == '1':
            self.logger.info("Updating planet resources")
            self.driver.find_element_by_xpath("//a[contains(@href, 'resources')]").click()
            #check building queue
            if self.is_building_queue_empty():
                if not self.should_upgrade_storage():
                    #get mine levels
                    self.get_mines_status(planet)
                    #decide next resource upgrade
                    self.choose_mine_upgrade()
            else:
                self.logger.info('Building Queue is NOT empty')

    def update_planet_fleet(self, planet):
        self.logger.info("UPDATING PLANET FLEET")

    def update_planet_stations(self, planet):
        if self.options['building']['enabled'] == '1':
            self.logger.info("Updating planet stations")
            self.driver.find_element_by_xpath("//a[contains(@href, 'station')]").click()
            if self.is_building_queue_empty():
                self.choose_station_upgrade()
            else:
                self.logger.info("Building Queue is NOT empty")
            

    def update_planet_researches(self, planet):
        self.logger.info("UPDATING PLANET RESEARCHES")

    def update_planet_hangar(self, planet):
        self.logger.info("UPDATING PLANET HANGAR QUEUE")

    def update_planet_defenses(self, planet):
        self.logger.info("UPDATING PLANET DEFENSE QUEUE")

    def handle_planets(self):
        self.fetch_planets()

        for p in iter(self.planet_id):
            self.navigate_to_planet(p)
            self.update_planet_info(p)
            self.update_planet_resources(p)
            self.update_planet_fleet(p)
            self.update_planet_stations(p)
            self.update_planet_researches(p)
            self.update_planet_hangar(p)
            self.update_planet_defenses(p)

        #for m in iter(self.moons):
        #    self.update_planet_info(m)
        #    self.update_planet_fleet(m)

    def work(self):
        self.driver.switch_to_window(self.worker_tab)
        self.handle_planets()
        return True
